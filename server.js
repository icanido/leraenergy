var express = require('express');
var xlsx = require('node-xlsx');
var ADODB = require('node-adodb');
ADODB.debug = true;
var app = express();

var request = require('request');
var energyRouter = require('./routes/energyRoutes')(express);

var bodyParser = require('body-parser');

var multer = require('multer');

var config = require('./libs/config');
var Energy = require('./libs/energy');
var dir = require('./libs/directory');

var Test = require('./libs/test');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({
    extended: true
})); // for parsing application/x-www-form-urlencoded


app.use(multer()); // for parsing multipart/form-data


// cache control
//var oneDay = 86400000;

app.use(express.static(__dirname + '/public'));
// cache control
//app.use(express.static(__dirname + '/public', {maxAge: oneDay}));



app.listen(config.get('port'), function () {
    console.info('Express server listening on port ' + config.get('port'));
});



// routing >>
var router = express.Router();

router.get('/api', function (req, res) {
    res.send('API is running');
});


app.use('/', router);


var gDay = (new Date()).getDate();
var gMonth = (new Date()).getMonth();
var gYear = (new Date()).getFullYear();



// routing >>
var router = express.Router();

//app.use('/', router);
app.use('/', energyRouter);



energyRouter.get('/', function (req, res) {
    res.send('Site is running');
});

energyRouter.get('/api', function (req, res) {
    res.send('API is running');
});



energyRouter.get('/api/getRegions', function (req, res) {
    console.info('/api/getRegions');

    var connection = ADODB.open(config.get('dbaccess'));

    connection
        .query('SELECT * FROM [Region];')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out);
        })
});





energyRouter.get('/api/getContractors/:iRegion', function (req, res) {
    console.info('/api/getContractors/:iRegion = ' + req.params.iRegion);

    var connection = ADODB.open(config.get('dbaccess'));

    connection
        .query('SELECT * FROM [Contractors] WHERE idRegion = ' + req.params.iRegion + ';')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out);
        })

});

//energyRouter.get('/api/getCompanies/:iRegion/:iContractor', function (req, res) {
energyRouter.get('/api/getCompanies/:iRegion', function (req, res) {
    console.info('/api/getCompanies/:iRegion = ' + req.params.iRegion);

    var connection = ADODB.open(config.get('dbaccess'));

    connection
        .query('SELECT * FROM [Company] WHERE idRegion = ' + req.params.iRegion + ';')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out);
        })
});



energyRouter.get('/api/getObjects/:iCompany', function (req, res) {
    console.info('/api/getObjects/:iCompany = ' + req.params.iCompany);

    var connection = ADODB.open(config.get('dbaccess'));

    connection
        .query('SELECT * FROM [qObjectsContract] WHERE idCompany = ' + req.params.iCompany + ';')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out);
        })
});






energyRouter.get('/api/getContracts/:iCompany', function (req, res) {
    console.info('/api/getContracts/:iCompany = ' + req.params.iRegion);

    var connection = ADODB.open(config.get('dbaccess'));

    connection
        .query('SELECT * FROM [Contracts] WHERE idCompany = ' + req.params.iCompany + ';')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out);
        })
});






// get questions from user profile
energyRouter.post('/api/getContractorsByObjects', function (req, res) {
    console.info('/api/getContractorsByObjects');

    var jsondata = JSON.parse(req.body.data);
    var objects = jsondata.idObjects;




    var strObjects = "(";

    objects.forEach(function (item, i) {
        strObjects += objects[i].id;

        if ((objects.length > 1) && (objects.length - 1 != i))
            strObjects += ", ";
    });
    strObjects += ")";



    var connection = ADODB.open(config.get('dbaccess'));


    connection
        .query('SELECT idContractors, nameContractor FROM [qContractors] WHERE idObjects in' + strObjects + 'GROUP BY idContractors, nameContractor;')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out.records);
        })
});




// get questions from user profile
energyRouter.post('/api/getContractsByContractors', function (req, res) {
    console.info('/api/getContractsByContractors');

    var jsondata = JSON.parse(req.body.data);

    var objects = jsondata.idObjects;
    var idContractors = jsondata.idContractors;


    var strObjects = "(";

    objects.forEach(function (item, i) {
        strObjects += objects[i].id;

        if ((objects.length > 1) && (objects.length - 1 != i))
            strObjects += ", ";
    });
    strObjects += ")";

    var connection = ADODB.open(config.get('dbaccess'));


    connection
        .query('SELECT idContracts, nameContract FROM [qContractors] WHERE (idObjects in' + strObjects + ' AND idContractors = ' + idContractors + ') GROUP BY idContracts, nameContract;')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out.records);
        })
});




energyRouter.get('/api/getPriceCategory/:nameObject', function (req, res) {
    console.info('/api/getPriceCategory/:nameObject = ' + req.params.nameObject);

    directory = new dir();
    iPriceCat = directory.PriceCategory(req.params.nameObject);

    var voltage = directory.Voltage(req.params.nameObject);

        var connection = ADODB.open(config.get('dbaccess'));

        connection
            .query('SELECT * FROM [PriceCategory] WHERE (id = ' + iPriceCat + ');')
            .on('done', function (out) {
                res.send([out.records[0].descr, voltage]);
            })
});



energyRouter.get('/api/getObjectsByContract/:iCompany/:iContract', function (req, res) {
    console.info('/api/getObjectsByContract/:iCompany = ' + req.params.iCompany);

    var connection = ADODB.open(config.get('dbaccess'));

    connection
        .query('SELECT * FROM [qObjectsContract] WHERE (idCompany = ' + req.params.iCompany + ' AND idContract = ' + req.params.iContract + ');')
        .on('done', function (out) {
            console.log('Result:', out);
            res.send(out);
        })
});



// Расчёт
energyRouter.post('/api/calc', function (req, res) {
    console.info('/api/calc');
    var jsondata = JSON.parse(req.body.data);

    var results = [];
    Energy(jsondata.sMonth, jsondata.sYear);
    jsondata.objects.forEach(function (item) {
        var resss = Energy.calc(item.nameObject);
        results.push(resss);
    });

    res.send(results);
});
