var config = require('./config');
var xlsx = require('node-xlsx');

var dirTotal = null;
var dirCalc = null;
var indexIncrease = null;
var ConsumptionEE = null;

//  This is constructor
var Directory = function () {
    const excelDir = xlsx.parse(config.get('excelDir'));

    dirTotal = excelDir[0];
    dirCalc = excelDir[1];
    indexIncrease = excelDir[2];
    ConsumptionEE = excelDir[3];
    ConsumptionPwr = excelDir[4];
    propertyObjects = excelDir[5];
}



// Объемы, МВт
Directory.prototype.M4 = function (nameObject, periodMonth, periodYear) {
    console.info('M4');

    var sFind = getTextMonth(periodMonth);

    var cYear = (getXYforTextContain(ConsumptionPwr.data, [sFind, periodYear]))[1]; // Столбец с нужным периодом
    var rObject = (getXYforTextContain(ConsumptionPwr.data, [nameObject]))[0]; // Строка с нужным объектом

    return ConsumptionPwr.data[rObject][cYear];
}



// Прямая цена покупки, руб./МВтч N4
Directory.prototype.N4 = function (periodMonth, periodYear) {
    console.info('N4');

    var sFind = getTextMonth(periodMonth);
    // получаем диапазон мощности для объекта
    var row = (getXYforTextContain(dirCalc.data, [sFind, periodYear]))[0];
    var col = (getXYforTextContain(dirCalc.data, ['расчеты на розничном рынке']))[1];


    var valueN4 = dirCalc.data[row][col];


    return valueN4;
}




// P4
Directory.prototype.P4 = function (nameObject, periodMonth, periodYear) {
    console.info('P4');

    var sFind = getTextMonth(periodMonth);
    // получаем диапазон мощности для объекта
    var row = (getXYforTextContain(dirCalc.data, [sFind, periodYear]))[0];
    var col = (getXYforTextContain(dirCalc.data, ['расчеты на розничном рынке']))[1];
    var valueP4_1 = dirCalc.data[row][col];

    // получаем диапазон мощности для объекта
    var row1 = (getXYforTextContain(propertyObjects.data, [nameObject]))[0];
    var col1 = (getXYforTextContain(propertyObjects.data, ['Диапазоны по мощности']))[1];
    var rangePower = propertyObjects.data[row1][col1];




    //Сбытовая надбавка
    var row2 = (getXYforTextContain(dirCalc.data, ['период']))[0] + 2;
    console.log(row2);

    var col2 = col + rangePower + (-3);
    console.log(col2);

    var valueP4_2 = dirCalc.data[row2][col2] / 100;

    return valueP4_1 * valueP4_2;
}



// Ценовая категория
Directory.prototype.PriceCategory = function (nameObject) {
    console.info('PriceCategory');
    console.info(nameObject);
    // получаем диапазон мощности для объекта
    var row1 = (getXYforTextContain(propertyObjects.data, [nameObject]))[0];
    var col1 = (getXYforTextContain(propertyObjects.data, ['Ценовая категория']))[1];

    console.log(row1);
    console.log(col1);

    var priceCat = propertyObjects.data[row1][col1];
    console.log(priceCat);

    return priceCat;
}




Directory.prototype.Voltage = function (nameObject) {
    var row = (getXYforTextContain(propertyObjects.data, [nameObject]))[0];
    var col = (getXYforTextContain(propertyObjects.data, ['Напряжение']))[1];
    var ret = propertyObjects.data[row][col];

    return ret;
}



// U4
Directory.prototype.U4 = function (nameObject, periodMonth, periodYear) {
    console.info('U4');

    // напряжение
    // получаем диапазон мощности для объекта
    //    var row = (getXYforTextContain(propertyObjects.data, [nameObject]))[0];
    //    var col = (getXYforTextContain(propertyObjects.data, ['Напряжение']))[1];
    //    var value1 = propertyObjects.data[row][col];


    var value1 = this.Voltage(nameObject);

    var sFind = getTextMonth(periodMonth);
    // получаем диапазон мощности для объекта
    var row1 = (getXYforTextContain(dirCalc.data, [sFind, periodYear]))[0];
    var col1 = (getXYforTextContain(dirCalc.data, [value1]))[1] + 1;
    var ret = dirCalc.data[row1][col1];

    return ret;
}









// Объемы, млн. кВтч
Directory.prototype.valueEE = function (nameObject, periodMonth, periodYear) {
    console.info('valueEE');

    var sFind = getTextMonth(periodMonth);

    var cYear = (getXYforTextContain(ConsumptionEE.data, [sFind, periodYear]))[1]; // Столбец с нужным периодом
    var rObject = (getXYforTextContain(ConsumptionEE.data, [nameObject]))[0]; // Строка с нужным объектом

    return ConsumptionEE.data[rObject][cYear];
}



// Прямая цена покупки, руб./МВтч
Directory.prototype.strenghtPriceForBuy = function (nameObject, periodMonth, periodYear) {
    console.info('strenghtPriceForBuy');

    // получаем диапазон мощности для объекта
    var rowRangePower = (getXYforTextContain(propertyObjects.data, [nameObject]))[0];
    var colRangePower = (getXYforTextContain(propertyObjects.data, ['Ценовая категория']))[1];
    var rangePower = propertyObjects.data[rowRangePower][colRangePower];

    console.log("rangePower");
    console.log(rangePower);


    var sFind = getTextMonth(periodMonth);

    var rowCalcPeriod = (getXYforTextContain(dirCalc.data, [sFind, periodYear]))[0];

    var colCalcCategory = 0;
    if (rangePower === 3 || rangePower === 4) {
        colCalcCategory = (getXYforTextContain(dirCalc.data, ['3,4 ценовая']))[1];
    } else {
        colCalcCategory = (getXYforTextContain(dirCalc.data, ['(' + rangePower + ')']))[1];
    }


    var ret = dirCalc.data[rowCalcPeriod][colCalcCategory];
    return ret;
}


// услуги по передаче, руб./МВтч
Directory.prototype.servicesForTransfer = function (nameObject, periodMonth, periodYear) {
    console.info('servicesForTransfer');

    var sFind = getTextMonth(periodMonth);

    var rowCalcPeriod = (getXYforTextContain(dirCalc.data, [sFind, periodYear]))[0];
    var colCalcCategory = (getXYforTextContain(dirCalc.data, ['СН-2(4)']))[1];

    var ret = dirCalc.data[rowCalcPeriod][colCalcCategory];
    return ret;
}


// услуги по передаче, руб./МВтч
Directory.prototype.salesIncrease = function (nameObject, periodMonth, periodYear) {
    console.info('salesIncrease');

    var sFind = getTextMonth(periodMonth);

    // получаем диапазон мощности для объекта
    var rowRangePower = (getXYforTextContain(propertyObjects.data, [nameObject]))[0];
    var colRangePower = (getXYforTextContain(propertyObjects.data, ['диапазоны']))[1];
    var rangePower = propertyObjects.data[rowRangePower][colRangePower];


    var rowCalcPeriod = (getXYforTextContain(dirCalc.data, [sFind, periodYear]))[0];
    var colCalcCategory = (getXYforTextContain(dirCalc.data, ['(Сб)', '(' + rangePower + ')']))[1];



    var ret = dirCalc.data[rowCalcPeriod][colCalcCategory];
    return ret;
}




//  Прочие услуги
Directory.prototype.otherServices = function (nameObject, periodMonth, periodYear) {
    console.info('otherServices');

    var sFind = getTextMonth(periodMonth);

    // Прочие услуги
    var rowRangePower = (getXYforTextContain(dirCalc.data, [sFind, periodYear]))[0];
    var colRangePower = (getXYforTextContain(dirCalc.data, ['Прочие услуги']))[1];
    var priceOtherServices = dirCalc.data[rowRangePower][colRangePower];

    console.log("priceOtherServices");
    console.log(priceOtherServices);


    // Индекс роста
    var rowindexIncrease = (getXYforTextContain(indexIncrease.data, ['Индекс роста']))[0];
    var colindexIncrease = (getXYforTextContain(indexIncrease.data, [periodYear]))[1];
    var valueIndexIncrease = indexIncrease.data[rowindexIncrease][colindexIncrease];
    valueIndexIncrease = valueIndexIncrease / 100;
    console.log("valueIndexIncrease");
    console.log(valueIndexIncrease);



    var ret = priceOtherServices * (1 + valueIndexIncrease);
    //var ret = priceOtherServices;
    return ret;
}









// поиск координат ячейки с вхождением массива переданых слов
getXYforTextContain = function (_sheet, _contain) {
    var ret = 'Unknown';

    _sheet.forEach(function (item, i) {

        try {
            item.forEach(function (itemStr, n) {
                var cntFindedString = 0;
                if (typeof (itemStr) == 'string') {
                    while (_contain.length > cntFindedString) {
                        var a = normVar(itemStr);
                        var b = normVar(_contain[cntFindedString]);

                        if (a.indexOf(b) > -1) {
                            cntFindedString++;
                            if (_contain.length == cntFindedString) {
                                ret = [i, n];
                            }
                        } else {
                            cntFindedString = 999; // выходим из цикла так как нет вхождения по одному из переданных слов
                        }
                    }
                } else if (typeof (itemStr) == 'number') {
                    while (_contain.length > cntFindedString) {
                        var a = itemStr;
                        var b = _contain[cntFindedString];

                        if (a == b) {
                            cntFindedString++;
                            if (_contain.length == cntFindedString) {
                                ret = [i, n];
                            }
                        } else {
                            cntFindedString = 999; // выходим из цикла так как нет вхождения по одному из переданных слов
                        }
                    }
                }
            });
        } catch (err) {
            console.error(err);
        }
    })

    return ret; // результат в виде Строка - Столбец
}



normVar = function (_a) {
    if (typeof (_a) == 'string') {
        _a = _a.toLowerCase();
    }
    return _a;
}



getTextMonth = function (iMonth) {
    var ret = 'Unknown';

    switch (iMonth) {
    case "1":
        ret = 'январь'
        break
    case "2":
        ret = 'февраль'
        break
    case "3":
        ret = 'март';
        break;
    case "4":
        ret = 'апрель';
        break;
    case "5":
        ret = 'май';
        break;
    case "6":
        ret = 'июнь';
        break;
    case "7":
        ret = 'июль';
        break;
    case "8":
        ret = 'август';
        break;
    case "9":
        ret = 'сентябрь';
        break;
    case "10":
        ret = 'октябрь';
        break;
    case "11":
        ret = 'ноябрь';
        break;
    case "12":
        ret = 'декабрь'
        break
    }

    return ret;
}



module.exports = Directory;
