var config = require('./config');
var dir = require('./directory');

var iMonth = 0;
var iYear = 0;

//  This is constructor
Energy = function (periodMonth, periodYear) {
    iMonth = periodMonth;
    iYear = periodYear;
}



Energy.calc = function (nameObject) {
    var B4 = valueEE(nameObject);
    var C4D4 = strenghtPriceForBuy(nameObject, B4);
    var E4F4 = servicesForTransfer(nameObject, B4);
    var G4H4 = salesIncrease(nameObject, B4);
    var IJKL4 = otherServices(nameObject, B4, C4D4[1], E4F4[1], G4H4[1]);


    // M N O P Q R S U V
    var M4 = fM4(nameObject);
    var NO4 = fNO4(nameObject, M4);
    var P4 = fP4(nameObject);
    var Q4 = (M4 * P4) / 1000;
    var S4 = NO4[1] + Q4;
    var R4 = (S4 / M4) * 1000;
    var U4 = fU4(nameObject);
    var V4 = M4*U4/1000;



    // W Y Z ...
    var Y = C4D4[1]+NO4[1];
    var X =  Y / B4;
    var AA = V4 + E4F4[1];
    var Z = AA / B4;
    var AC = G4H4[1] + Q4;
    var AB = AC / B4;
    var AE = IJKL4[1];
    var AC = G4H4[1]+Q4;
    var AD = AE / B4;
    var AG = Y+AA+AC+AE;
    var AF = AG / B4;



    var result = {
        nameObject: nameObject,
        B4: B4.toFixed(5),
        C4: C4D4[0].toFixed(2),
        D4: C4D4[1].toFixed(5),
        E4: E4F4[0].toFixed(2),
        F4: E4F4[1].toFixed(5),
        G4: G4H4[0].toFixed(2),
        H4: G4H4[1].toFixed(5),
        I4: IJKL4[0].toFixed(2),
        J4: IJKL4[1].toFixed(5),
        K4: IJKL4[2].toFixed(5),
        L4: IJKL4[3].toFixed(5),
        M4: M4.toFixed(5),
        N4: NO4[0].toFixed(2),
        O4: NO4[1].toFixed(2),
        P4: P4.toFixed(2),
        Q4: Q4.toFixed(2),
        R4: R4.toFixed(2),
        S4: S4.toFixed(2),
        U4: U4.toFixed(3),
        V4: V4.toFixed(5),
        W: B4.toFixed(5),
        X: X.toFixed(5),
        Y: Y.toFixed(2),
        Z: Z.toFixed(5),
        AA: AA.toFixed(2),
        AB: AB.toFixed(2),
        AC: AC.toFixed(2),
        AD: AD.toFixed(5),
        AE: AE.toFixed(5),
        AF: AF.toFixed(5),
        AG: AG.toFixed(2)
    };

    return result;
}






// Объемы, млн. кВтч = B4
var valueEE = function (nameObject) {
    var ret = false;
    var err = null;

    directory = new dir();

    try {
        var B4 = directory.valueEE(nameObject, iMonth, iYear);
        ret = B4;
    } catch (error) {
        console.log(error);
        ret = false;
    }
    return B4;
}



// прямая цена покупки, руб./МВтч = C4, D4
var strenghtPriceForBuy = function (nameObject, _B4) {
    var ret = [];
    var err = null;

    directory = new dir();

    try {
        var C4 = directory.strenghtPriceForBuy(nameObject, iMonth, iYear);
        ret[0] = C4; //C4

        ret[1] = _B4 * C4; //D4
    } catch (error) {
        console.log(error);
        ret[0] = false;
        ret[1] = false;
    }
    return ret;
}



// услуги по передаче, руб./МВтч = E4, F4
var servicesForTransfer = function (nameObject, _B4) {
    var ret = [];
    var err = null;

    directory = new dir();

    try {
        var E4 = directory.servicesForTransfer(nameObject, iMonth, iYear);
        ret[0] = E4; //E4
        ret[1] = _B4 * E4; //F4
    } catch (error) {
        console.log(error);
        ret[0] = false;
        ret[1] = false;
    }
    return ret;
}



// сбытовая надбавка ГП, руб./МВтч = G4
var salesIncrease = function (nameObject, _B4) {
    var ret = [];
    var err = null;

    directory = new dir();

    try {
        var G4 = directory.salesIncrease(nameObject, iMonth, iYear);
        ret[0] = G4;
        ret[1] = G4 * _B4; //H4
    } catch (error) {
        console.log(error);
        ret[0] = false;
        ret[1] = false;
    }
    return ret;
}




// Прочие услуги = I4, J4, L4, K4
var otherServices = function (nameObject, _B4, _D4, _F4, _H4) {
    var ret = [];
    var err = null;

    directory = new dir();

    try {
        var I4 = directory.otherServices(nameObject, iMonth, iYear);
        ret[0] = I4; //I4
        ret[1] = _B4 * I4; //J4
        ret[3] = _D4 + _F4 + _H4 + ret[1]; //L4
        ret[2] = ret[3]/_B4; //K4

    } catch (error) {
        console.log(error);
        ret[0] = false;
        ret[1] = false;
        ret[2] = false;
        ret[3] = false;
    }
    return ret;
}



// Объемы, МВт = M4
var fM4 = function (_nameObject) {
    var ret = false;
    var err = null;

    directory = new dir();

    try {
        var M4 = directory.M4(_nameObject, iMonth, iYear);
        ret = M4;
    } catch (error) {
        console.log(error);
        ret = false;
    }
    return M4;
}



// Ценовая категория
var fPriceCategory = function (_nameObject) {
    var ret = false;
    var err = null;

    directory = new dir();

    try {
        var pc = directory.PriceCategory(_nameObject);
        ret = pc;
    } catch (error) {
        console.log(error);
        ret = false;
    }
    return ret;
}



// Объемы, МВт = N4
var fNO4 = function (_nameObject, _M4) {
    var ret = [];
    var err = null;

    directory = new dir();

    try {
        var N4 = directory.N4(iMonth, iYear);
        ret[0] = N4;
        ret[1] = (_M4 * N4) / 1000;
    } catch (error) {
        console.log(error);
        ret = false;
    }
    return ret;
}



// Объемы, МВт = O4
var fP4 = function (_nameObject) {
    var ret = false;
    var err = null;

    directory = new dir();

    try {
        var P4 = directory.P4(_nameObject, iMonth, iYear);
        ret = P4;
    } catch (error) {
        console.log(error);
        ret = false;
    }
    return P4;
}



// Объемы, МВт = U4
var fU4 = function (_nameObject) {
    var ret = false;
    var err = null;

    directory = new dir();

    try {
        var U4 = directory.U4(_nameObject, iMonth, iYear);
        ret = U4;
    } catch (error) {
        console.log(error);
        ret = false;
    }
    return U4;
}


module.exports = Energy;
