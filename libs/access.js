var ADODB = require('node-adodb');
ADODB.debug = true;

var config = require('./config');


db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback() {
    log.info("Connected to DB!");
});



// store for all value from arduino sensor
var sensorSheme = new mongoose.Schema({
    sensorName: String,
    board: String,
    boardValueId: Number,
    sensorValue: Number,
    updateTime: {
        type: Date,
        default: Date.now
    }
});

var sensorModel = mongoose.model('sensorValue', sensorSheme);


// store for all pay and automatic send information
// service [pw-power, wth-water hot, wtc-water cold]
var journalPayScheme = new mongoose.Schema({
    seen: {
        type: Boolean,
        default: false
    },
    service: String,
    svcValue: Number,
    svcCost: Number,
    svcTestimony: Number,
    svcSendMethod: {
        type: String,
        default: 'email'
    },
    updateTime: {
        type: Date,
        default: Date.now
    }
});
var journalPayModel = mongoose.model('journalPay', journalPayScheme);






// list all models >>
module.exports.journalPayModel = journalPayModel;
module.exports.sensorModel = sensorModel;
