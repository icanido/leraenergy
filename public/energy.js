'use strict';

var energyApp = angular.module('energyApp', ['ngRoute', 'ui.bootstrap']);

energyApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/step1.htm',
            controller: 'step1Ctrl'
        })
        .when('/step1', {
            templateUrl: 'views/step1.htm',
            controller: 'step1Ctrl'
        })
        .when('/step2', {
            templateUrl: 'views/step2.htm',
            controller: 'step2Ctrl'
        })
        .when('/step3', {
            templateUrl: 'views/step3.htm',
            controller: 'step3Ctrl'
        })
        .when('/power', {
            templateUrl: 'views/power.htm',
            controller: 'powerCtrl'
        })
        .when('/report/:doit', {
            templateUrl: 'views/report.htm',
            controller: 'reportCtrl'
        })
        .when('/weather', {
            templateUrl: 'views/weather.htm',
            controller: 'weatherCtrl'
        })
        .when('/analytics', {
            templateUrl: 'views/analytics.htm',
            controller: 'analyticsCtrl'
        });
});



energyApp.filter('more', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});
