/*power.html*/
energyApp.controller('powerCtrl', function ($q, $http, $scope, $rootScope, dbFactory, calcFactory, $timeout, $filter) {
    $scope.$parent.headerCaption = "Smart homa - POWER";

    var interval = 1;
    $scope.perMonth = " ...";
    $scope.socialNormOurFamily = " ...";
    $scope.perMonthRub = " ...";
    $scope.tarifPowerSocNorm = " ...";
    $scope.tarifPowerOverSocNorm = " ...";



    //    https://github.com/stherrienaspnet/ngRadialGauge >>
    $scope.gaugeValue = 0;
    $scope.upperLimit = 5000;
    $scope.lowerLimit = 0;
    $scope.unit = " Watt";
    $scope.precision = 1;



    $scope.ranges = [
        {
            min: 0,
            max: 210,
            color: '#c7c7c7'
            },
        {
            min: 210,
            max: 500,
            color: '#8DCA2F'
            },
        {
            min: 500,
            max: 3000,
            color: '#FDC702'
        },
        {
            min: 3000,
            max: 4000,
            color: '#d68239'
            },
        {
            min: 4000,
            max: 5000,
            color: '#c74241'
            }
        ];


    // get lonely data of power at now time
    function querydata() {
        dbFactory.lastValuePower().then(function (output) {
            if ($scope.gaugeValue != Number(output.sensorValue)) {
                $scope.gaugeValue = Number(output.sensorValue);
            }
        });
    }
    //    https://github.com/stherrienaspnet/ngRadialGauge <<




    // set contant for module
    function setConstant() {
        calcFactory.getConfParam('powerSocNorm').then(function (out) {
            $scope.socialNormOurFamily = Number(out);

            calcFactory.getConfParam('tarifPowerSocNorm').then(function (out) {
                $scope.tarifPowerSocNorm = Number(out);

                calcFactory.getConfParam('tarifPowerOverSocNorm').then(function (out) {
                    $scope.tarifPowerOverSocNorm = Number(out);
                });
            });
        });
    }






    // buils chart plan - fact
    function buildArrPlanFactPerMonth(_month) {
        dbFactory.firstByMonthByDayPower(_month).then(function (out) {
            var dt = new Date();
            var firstDay = new Date(dt.getFullYear(), _month, 1);
            var lastDay = new Date(dt.getFullYear(), _month + 1, 0);
            $scope.chartForPlanConsumpsionPerMonth = [
                {
                    values: [{
                        'x': firstDay,
                        'y': 0
                    }, {
                        'x': lastDay,
                        'y': 150
                    }],
                    key: 'plan',
                    color: '#5f8d74'
                }
            ];

            var powerFact = {
                values: [{
                    'x': firstDay,
                    'y': 0
                                }],
                key: 'fact',
                color: '#c76116'
            };



            // get only first value of day, because very lot items ...
            out.forEach(function (item) {
                if (dt.getDay() != (new Date(item.updateTime)).getDay()) // change date, then keep value
                {
                    dt = new Date(item.updateTime);
                    powerFact.values.push({
                        'x': new Date(item.updateTime),
                        'y': parseInt((item.boardValueId - out[0].boardValueId) / 1000)
                    });
                }
            });

            powerFact.values.push({
                'x': new Date(out[out.length - 1].updateTime),
                'y': parseInt((out[out.length - 1].boardValueId - out[0].boardValueId) / 1000)
            });

            $scope.chartForPlanConsumpsionPerMonth.push(powerFact);
        });
    }







    // calculate Power per month >>
    function calcPowerPerMonth() {
        var _idLast = 0,
            _idFisrt = 0;

        dbFactory.lastValuePower().then(function (output) {
            _idLast = Number(output.boardValueId);

            var d = new Date();
            var month = d.getMonth();

            dbFactory.firstByMonthPower(month).then(function (output) {
                _idFisrt = Number(output.boardValueId);
                $scope.perMonthPower = parseInt((_idLast - _idFisrt) / 1000);

                // calculate power rub >>
                if ($scope.perMonthPower > $scope.socialNormOurFamily) {
                    $scope.perMonthRub = ($scope.perMonthPower - $scope.socialNormOurFamily) * $scope.tarifPowerOverSocNorm;
                    $scope.perMonthRub = parseInt($scope.perMonthRub + ($scope.socialNormOurFamily * $scope.tarifPowerSocNorm));
                } else {
                    $scope.perMonthRub = parseInt($scope.perMonthPower * $scope.tarifPowerSocNorm);
                }
                // calculate power rub <<

                buildArrPlanFactPerMonth(month); // buils chart plan - fact
            });
        });
    }


    // calculate Power per month <<



    //    https://github.com/n3-charts/line-chart >>
    $scope.optionsPowerLine = {
        lineMode: "linear",
        axes: {
            x: {
                type: "date",
                key: "updateTime"
            },
            y: {
                min: 0,
            }
        },
        series: [
            {
                y: "sensorValue",
                label: "Interpolation power consumption",
                color: "#a23d1e",
                thickness: '2px',
                type: 'line',
                dotSize: 0
            }, {
                y: "sensorValue",
                label: "Pulse power consumption",
                type: "column",
                color: "#313131"
            }
        ],
        tooltip: {
            mode: "scrubber"
        },
        drawLegend: true,
        drawDots: true,
        hideOverflow: false,
        columnsHGap: 5
    };


    // get data of power by set interval
    function lastHourPower() {
        dbFactory.lastHourPower(interval).then(function (output) {

            console.info(output);

            $scope.dataPower = output;
            $scope.dataPower.forEach(function (row) {
                row.updateTime = new Date(row.updateTime);
            });
        });
    }
    //    https://github.com/n3-charts/line-chart <<






    ///////////////////////////////////////////////////////////////////////////


    $scope.options = {
        chart: {
            type: 'lineChart',
            height: 235,
            margin: {
                top: 20,
                right: 20,
                bottom: 40,
                left: 65
            },
            x: function (d) {
                return d.x;
            },
            y: function (d) {
                return d.y;
            },
            y1: function (d) {
                return d.y;
            },
            useInteractiveGuideline: true,
            xAxis: {
                axisLabel: 'Days',
                tickFormat: function (d) {
                    return d3.time.format('%d')(new Date(d))
                }
            },
            yAxis: {
                axisLabel: 'Power (kW)',
                axisLabelDistance: 0
            }
        }
    };







    // chart for planing consumption energy >>
    setConstant();
    $scope.gaugeValue = 0; // init query data
    lastHourPower(); // get data of power by set interval

    calcPowerPerMonth();

    // set interval to show data power on a line chart
    $scope.setInterv = function (_interval) {
        interval = Number(_interval);
        lastHourPower();
    }









    // for timer >>
    setInterval(querydata, 1000);
    setInterval(lastHourPower, 60000);
    setInterval(calcPowerPerMonth, 3600000);
    // function for blocking grow consumption memory
    //    $scope.$on("$destroy", function () {
    //        clearTimeout(querydata);
    //        clearTimeout(lastHourPower);
    //        clearTimeout(calcPowerPerMonth);
    //    });

});
