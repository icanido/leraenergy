/*index.html*/
energyApp.controller('step1Ctrl', function ($q, $http, $scope, dbFactory, dataForCalc) {


    $scope.forCalculate = {
        objects: [],
        periodYear: 0,
        periodMonth: 0,
        selectCompany: null
    }

    dbFactory.getTable('getRegions').then(function (regions) {
        $scope.regions = regions;
    })


    // выбран регион
    $scope.changeRegion = function () {
        if ($scope.selectRegion !== null) {
                 dbFactory.getCompanies($scope.selectRegion.id).then(function (out) {
                $scope.companies = out;
            })
        }
    }


       // Выбрано общество
    $scope.changeCompany = function () {
        if (($scope.selectRegion !== null) && ($scope.selectCompanies !== null)) {
            dataForCalc.setCompany($scope.selectCompany.id);
            dbFactory.getObjects($scope.selectCompany.id).then(function (out) {
                dataForCalc.setObjects(out);
            })
        }
    }
});
