energyApp.controller('step2Ctrl', function ($q, $http, $scope, $location, dbFactory, dataForCalc, resultsCalc) {
    $scope.forCalculate = {
        objects: [],
        periodYear: 0,
        periodMonth: 0
    }

    $scope.forCalculate = dataForCalc.getData();

    console.log(dataForCalc.getData());

    var formData = {
        'idObjects': dataForCalc.getData().objects
    };

    var jdata = 'data=' + JSON.stringify(formData);


    dbFactory.getContractorsByObjects(jdata).then(function (out) {
        $scope.contractors = out;
    })



    // Выбран контрагент
    $scope.changeContractors = function () {

        var formData = {
            'idObjects': dataForCalc.getData().objects,
            'idContractors': $scope.selectContractors.idContractors
        };

        var jdata = 'data=' + JSON.stringify(formData);


        dbFactory.getContractsByContractors(jdata).then(function (out) {
            $scope.contracts = out;
        })
    }






    // Выбран договор
    $scope.changeContract = function () {
        var formData = {
            'idObjects': dataForCalc.getData().objects,
            'idContractors': $scope.selectContractors.idContractors
        };

        var jdata = 'data=' + JSON.stringify(formData);

        dbFactory.getObjectsByContract(dataForCalc.getData().iCompany, $scope.selectContract.idContracts).then(function (out) {
            $scope.forCalculate.objects = [];
            dataForCalc.setObjects(out);
        //    $scope.forCalculate.objects = out;
        })
    }






    $scope.calc = function () {
        var formData = {
            'objects': dataForCalc.getData().objects,
            'sYear': $scope.forCalculate.periodYear,
            'sMonth': $scope.forCalculate.periodMonth
        };

        var jdata = 'data=' + JSON.stringify(formData);

        var def = $q.defer();

        $http({
                method: 'POST',
                url: '/api/calc',
                data: jdata,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function (response) {
                resultsCalc.setResult(response);

                def.resolve(response);
                $location.path('/step3');
            })
            .error(function (response) {
                console.log(response);
            });

        return def.promise;
    }
})
