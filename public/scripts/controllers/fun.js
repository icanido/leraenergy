energyApp.controller('TimeCtrl',
    function ($scope, $timeout) {
        $scope.clock = "loading clock..."; // initialise the time variable
        $scope.tickInterval = 1000; //ms

        var tick = function () {
            $scope.clock = Date.now(); // get the current time
            $timeout(tick, $scope.tickInterval); // reset the timer
        }

        // Start the timer
        $timeout(tick, $scope.tickInterval);
    });


energyApp.controller('TempCtrl',
    function ($scope, dbFactory, $timeout) {
        $scope.tickIntervalTemp = 60000; //ms
        $scope.temp = "...";


        var tick = function () {

            dbFactory.getTemp().then(function (output) {
                $scope.temp = output;
            });
            $timeout(tick, $scope.tickIntervalTemp); // reset the timer
        }

        // Start the timer
        tick();
    });
