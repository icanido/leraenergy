homaApp.factory('calcFactory', function ($http, $q, $filter) {
    var db = null;

    return {
        getConfParam: function (parm) {
            var def = $q.defer();

            $http.get('/api/configParam/' + parm)
                .success(function (out) {
                    def.resolve(out.ParamValue);
                })
                .error(function (out, status, headers, config) {
                    def.reject('Error in http request');
                });

            return def.promise;
        }
    }
});
