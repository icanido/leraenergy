energyApp.service('resultsCalc', function () {

    var result = [];



    setResult = function (_result) {
        result = _result;
    };

    getResults = function () {
        return result;
    };


    return {
        getResults: getResults,
        setResult: setResult
    };
});
