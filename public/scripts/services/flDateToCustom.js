//2015-09-08T09:52:44.018Z
//[month, days, hour, minute, second]
homaApp.filter('DateToCustom', function () {
    return function DateTo(_data, _format) {
        console.info("_format:" + _format);
        var ret = [];

        switch (_format) {
        case 'month':
            vret = String(_timeStamp.split("-")[1]);
            break
        case 'days':
            vret = String(_timeStamp.split("-")[2].substring(0, 2));
            break
        case 'hour':
            for (var i = 0; i < _data.length; i++) {
                var _timeStamp = _data[i].updateTime;
                vret = 7 + Number(_timeStamp.split("T")[1].substring(0, 2));
                ret[i] = {
                    'updateTime': vret,
                    'sensorValue': _data[i].sensorValue
                };
            }
            break
        case 'minute':
            var n = 0;
            var vretCaptionX = "",
                vretValueX = "",
                valueSen = 0,
                countVal = 0;

            for (var i = 0; i < _data.length; i++) {

                var _timeStamp = _data[i].updateTime;

                vretHour = 7 + Number(_timeStamp.split("T")[1].substring(0, 2));
                vretMinute = String(_timeStamp.split(":")[1]);

                if ((vretCaptionX != vretHour + ":" + vretMinute) && (vretCaptionX != "")) {
                    ret[n] = {
                        'captionX': vretCaptionX,
                        'sensorValue': parseInt(valueSen / countVal),
                        'updateTime': parseFloat(vretValueX)
                    };
                    n++;
                    valueSen = 0;
                    countVal = 0;
                }

                vretCaptionX = vretHour + ":" + vretMinute;
                if (parseInt(vretMinute) == 0) {
                    vretValueX = vretHour + "." + 0;
                } else {
                    if ((vretMinute * 100) / 60 < 10)
                        vretValueX = vretHour + ".0" + parseInt((vretMinute * 100) / 60);
                    else
                        vretValueX = vretHour + "." + parseInt((vretMinute * 100) / 60);
                }

                valueSen = valueSen + _data[i].sensorValue;
                countVal++;
            }

            console.info(ret);
            break
        case 'second':
            vret = String(_timeStamp.split(":")[2].substring(0, _timeStamp.split(":")[2].length() - 1));
            break
        default:
            vret = 0;
            break
        }


        return ret;
    };
});
