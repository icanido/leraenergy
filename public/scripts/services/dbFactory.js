energyApp.factory('dbFactory', function ($http, $q, $filter) {
    var db = null;

    return {


        // take types of organizations
        getTable: function (apiName) {
            var def = $q.defer();

            $http.get('/api/' + apiName)
                .success(function (data) {
                    console.log(data.records);
                    def.resolve(data.records);
                })
                .error(function (data, status, headers, config) {
                    def.reject('Error in http request');
                    console.log(data);
                    console.log(status);
                });

            return def.promise;
        },






        // take types of organizations
        getContractors: function (sRegion) {
            var def = $q.defer();
            $http.get('/api/getContractors/' + sRegion)
                .success(function (data) {
                    console.log(data.records);
                    def.resolve(data.records);
                })
                .error(function (data, status, headers, config) {
                    def.reject('Error in http request');
                    console.log(data);
                    console.log(status);
                });

            return def.promise;
        },



        // take types of organizations
        getCompanies: function (iRegion, iContractor) {
            var def = $q.defer();
            $http.get('/api/getCompanies/' + iRegion)
                .success(function (data) {
                    console.log(data.records);
                    def.resolve(data.records);
                })
                .error(function (data, status, headers, config) {
                    def.reject('Error in http request');
                    console.log(data);
                    console.log(status);
                });

            return def.promise;
        },



        // get questions from user profile
        getContractorsByObjects: function (jdata) {
            var def = $q.defer();

            $http({
                    method: 'POST',
                    url: '/api/getContractorsByObjects',
                    data: jdata,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function (response) {
                    def.resolve(response);
                })
                .error(function (response) {
                    console.log(response);
                });

            return def.promise;
        },

        // get questions from user profile
        getContractsByContractors: function (jdata) {
            var def = $q.defer();

            $http({
                    method: 'POST',
                    url: '/api/getContractsByContractors',
                    data: jdata,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function (response) {
                    def.resolve(response);
                })
                .error(function (response) {
                    console.log(response);
                });

            return def.promise;
        },
        
        
        
        // take types of organizations
        getObjectsByContract: function (iCompany, iContract) {
            var def = $q.defer();
            $http.get('/api/getObjectsByContract/' + iCompany + '/' + iContract)
                .success(function (data) {
                    console.log(data.records);
                    def.resolve(data.records);
                })
                .error(function (data, status, headers, config) {
                    def.reject('Error in http request');
                    console.log(data);
                    console.log(status);
                });

            return def.promise;
        },



        // take types of organizations
        getObjects: function (iCompany) {
            var def = $q.defer();
            $http.get('/api/getObjects/' + iCompany)
                .success(function (data) {
                    console.log(data.records);
                    def.resolve(data.records);
                })
                .error(function (data, status, headers, config) {
                    def.reject('Error in http request');
                    console.log(data);
                    console.log(status);
                });

            return def.promise;
        },



        // take types of organizations
        getDescrForPriceCategory: function (_nameObject) {
            var ret = '';
            var def = $q.defer();
            $http.get('/api/getPriceCategory/' + _nameObject)
                .success(function (data) {
                    console.log(data);
                    ret = def.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    def.reject('Error in http request');
                    console.log(data);
                    console.log(status);
                });

            return def.promise;
        },



        // take types of organizations
        getContracts: function (iCompany) {
            var def = $q.defer();
            $http.get('/api/getContracts/' + iCompany)
                .success(function (data) {
                    console.log(data.records);
                    def.resolve(data.records);
                })
                .error(function (data, status, headers, config) {
                    def.reject('Error in http request');
                    console.log(data);
                    console.log(status);
                });

            return def.promise;
        }
    }
});
