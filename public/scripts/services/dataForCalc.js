energyApp.service('dataForCalc', function ($http, $q, dbFactory) {

    var dfc = {
        objects: [],
        periodYear: 0,
        periodMonth: 0,
        iCompany: 0
    }

    setObjects = function (newObjs) {
         dfc.objects.clear;

        newObjs.forEach(function(obj) {
            dbFactory.getDescrForPriceCategory(obj.nameObject).then(function (out) {
                obj.priceCategory = out[0];
                obj.voltage = out[1];
                dfc.objects.push(obj);
            })
        });
    };

    setYear = function (iYear) {
        dfc.periodYear = iYear;
    };

    setMonth = function (iMonth) {
        dfc.periodMonth = iMonth;
    };

       setCompany = function (iCompany) {
        dfc.iCompany = iCompany;
    };




    getData = function () {
        return dfc;
    };

    getObjects = function() {
        return dfc.objects;
    }

    return {
        getData: getData,
        setObjects: setObjects,
        setYear: setYear,
        setMonth: setMonth,
        setCompany: setCompany
    };
});
