var path = require('path');


module.exports = function energyRouter(express) {

    var energyRouter = express.Router();

    /*Redirect user to AngularJs App*/
    var appFolder = path.join(__dirname, '../public');
    energyRouter.use(express.static(appFolder));
    energyRouter.get('/', function (req, res) {
        res.sendFile('index.html', {
            root: appFolder
        });
    });
    return energyRouter;

};
